﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParGo.Shared.Validation
{
    /// <summary>
    /// Usage due to stack filtering that is done to get a small and refined stacktrace
    /// where the validation error occured
    /// </summary>
    [Serializable]
    public sealed class ValidationError : ErrorInfo
    {
        private string _errorGroup;
        /// <summary>
        /// In which error group the exception belongs to
        /// </summary>
        public override string ErrorGroup
        {
            get { return _errorGroup; }
            set { _errorGroup = value; }
        }

        private string _errorCode;
        /// <summary>
        /// What is the error code of the exception
        /// </summary>
        public override string ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        private string _errorMessage;
        /// <summary>
        /// What is the error message
        /// </summary>
        public override string UserVisibleMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        /// <summary>
        /// CTOR for future use, when and if error codes are introduced, then will remove CTOR with error message
        /// </summary>
        /// <param name="combinedErrorGroupCode"></param>
        //public ValidationError(string combinedErrorGroupCode)
        //{
        //    ErrorGroup = DetectErrorGroup(combinedErrorGroupCode);
        //    ErrorCode = DetectErrorCode(combinedErrorGroupCode);
        //}

        /// <summary>
        /// CTOR
        /// </summary>
        /// <param name="errorGroup"></param>
        /// <param name="errorCode"></param>
        public ValidationError(string errorGroup, string errorCode)
        {
            ErrorGroup = errorGroup;
            ErrorCode = errorCode;
        }
        /// <summary>
        /// CTOR
        /// </summary>
        /// <param name="errorMessage"></param>
        public ValidationError(string errorMessage)
        {
            UserVisibleMessage = errorMessage;
        }

        /// <summary>
        /// Detect and retrieve an error group
        /// </summary>
        /// <param name="combinedErrorGroupCode"></param>
        /// <returns></returns>
        private string DetectErrorGroup(string combinedErrorGroupCode)
        {
            if (string.IsNullOrEmpty(combinedErrorGroupCode)) return null;

            var index = 0;
            for (var i = 0; i < combinedErrorGroupCode.Length; i++)
            {
                if (!char.IsNumber(combinedErrorGroupCode[i]))
                {
                    index = i;
                }
                else
                {
                    break;
                }
            }

            return combinedErrorGroupCode.Substring(0, index + 1);
        }

        /// <summary>
        /// Detect and retrieve error code
        /// </summary>
        /// <param name="combinedErrorGroupCode"></param>
        /// <returns></returns>
        private string DetectErrorCode(string combinedErrorGroupCode)
        {
            if (string.IsNullOrEmpty(combinedErrorGroupCode)) return null;

            var index = 0;
            for (var i = 0; i < combinedErrorGroupCode.Length; i++)
            {
                if (char.IsNumber(combinedErrorGroupCode[i]))
                {
                    index = i;
                    break;
                }
            }

            return combinedErrorGroupCode.Substring(index, combinedErrorGroupCode.Length - index);
        }
    }
}
