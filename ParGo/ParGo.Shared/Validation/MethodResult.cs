﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParGo.Shared.Validation
{
    /// <summary>
    /// Represents method result class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class MethodResult<T> : VoidResult
    {
        /// <summary>
        /// Output of the method execution
        /// </summary>
        public T Result { get; set; }

        #region CTORS

        public MethodResult() { }

        public MethodResult(ErrorType errorType = ErrorType.None) : base(errorType) { }

        public MethodResult(T result)
        {
            Result = result;
        }

        public MethodResult(MethodResult<T> result)
            : base(result)
        {
            Result = result.Result;
        }

        public MethodResult(List<ErrorInfo> errorInfos, ErrorType errorType)
        {
            ErrorInfos = errorInfos;
            ErrorType = errorType;
        }

        #endregion
    }

    public static class MethodResult
    {
        public static MethodResult<T> Create<T>(T item)
        {
            return new MethodResult<T>(item);
        }
    }
}

