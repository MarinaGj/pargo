﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParGo.Shared.Validation
{
    /// <summary>
    /// Represents void result
    /// </summary>
    [Serializable]
    public class VoidResult
    {
        /// <summary>
        /// Name of the method called
        /// </summary>
        public string MethodName { get; set; }

        /// <summary>
        /// Error info collection that occured during method execution
        /// </summary>
        public List<ErrorInfo> ErrorInfos { get; protected set; }

        /// <summary>
        /// Define type of the error that occured
        /// </summary>
        public ErrorType ErrorType { get; set; }

        /// <summary>
        /// Indicates that the execution have been faulter
        /// </summary>
        public bool IsFaulted
        {
            get { return ErrorInfos.Any(); }
        }

        /// <summary>
        /// Aggregate all error messages from error infos into one string
        /// </summary>
        public string ErrorInfoAggregatedMessage
        {
            get
            {
                if (ErrorInfos.Count == 0) return string.Empty;
                if (ErrorInfos.Count == 1) return ErrorInfos[0].Message;
                return ErrorInfos.Select(x => x.Message).Aggregate((a, b) => a + Environment.NewLine + b);
            }
        }

        /// <summary>
        /// Aggregate all user visible error messages from error infos into one string
        /// </summary>
        public string ErrorInfoAggregatedUserVisibleMessage
        {
            get
            {
                if (ErrorInfos.Count == 0) return string.Empty;
                if (ErrorInfos.Count == 1) return ErrorInfos[0].UserVisibleMessage;
                return ErrorInfos.Select(x => x.UserVisibleMessage).Aggregate((a, b) => a + Environment.NewLine + b);
            }
        }

        public VoidResult()
        {
            ErrorInfos = new List<ErrorInfo>();
        }

        public VoidResult(ErrorType errorType = ErrorType.None)
        {
            ErrorInfos = new List<ErrorInfo>();
            ErrorType = errorType;
        }

        public VoidResult(VoidResult result)
        {
            ErrorInfos = result.ErrorInfos;
            ErrorType = result.ErrorType;
        }
    }

    [Flags]
    public enum ErrorType
    {
        None = 0,
        System = 1,
        Business = 2,
    }

}


