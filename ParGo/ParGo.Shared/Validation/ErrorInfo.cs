﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParGo.Shared.Validation
{
    /// <summary>
    /// Error info class used for error descriptions
    /// </summary>
    [Serializable]
    public abstract class ErrorInfo
    {
        public Exception Exception { get; set; }

        /// <summary>
        /// Represents the system error message
        /// </summary>
        public virtual string Message { get; set; }

        /// <summary>
        /// Represents the user visible error message
        /// </summary>
        public virtual string UserVisibleMessage { get; set; }

        /// <summary>
        /// Represents the error code of the error
        /// </summary>
        public virtual string ErrorGroup { get; set; }

        /// <summary>
        /// Represents the error code of the error
        /// </summary>
        public virtual string ErrorCode { get; set; }

        /// <summary>
        /// CTOR
        /// </summary>
        /// <param name="ex"></param>
        protected ErrorInfo(Exception ex = null)
        {
            this.Exception = ex;
        }

        public virtual string GetFormatedMessage()
        {
            return string.Format("ErrorGroup: {0} ErrorCode: {1} Message:{2} InnerException:{3}", ErrorGroup, ErrorCode, Message, Exception);
        }

        /// <summary>
        /// Return the combined error groupe + error code
        /// </summary>
        /// <returns></returns>
        public virtual string GetErrorIdentifier()
        {
            return string.Concat(ErrorGroup ?? "GE", ErrorCode ?? @"10000");
        }
    }
}
