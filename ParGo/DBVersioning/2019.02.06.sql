﻿BEGIN TRANSACTION;

DECLARE @Version decimal(5,2) = 1.01;

IF ((SELECT [dbo].[CheckVersion] (@Version)) = 0)
BEGIN
ROLLBACK;
RAISERROR('CURRENT VERSION IS GREATER OR SAME THAN THE ONE YOU ARE TRYING TO EXECUTE', 20,-1) with log;
END

--YOUR SQL CODE GOES HERE, DO NOT USE GO INSTEAD USE ;
IF OBJECT_ID(N'dbo.User', N'U') IS  NULL
BEGIN
CREATE TABLE [dbo].[User](
	[Id] [uniqueidentifier] NOT NULL,
	[PhoneNumber] [nvarchar](20) NOT NULL,
	[DeviceId] [nvarchar](20) NOT NULL,
	[LicencePlate] [nvarchar](10) NULL,
	[HomeAddress] [nvarchar](100) NULL,
	[VerificationCode] [int] NULL,
	[IsVerified] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
--YOUR SQL CODE ENDS HERE

DELETE from DBVersion;

INSERT INTO DBVersion(VersionNumber) VALUES (@Version);

print 'SCRIPT EXECUTION COMPLETED SUCESSFULLY';

COMMIT;
