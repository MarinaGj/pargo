﻿BEGIN TRANSACTION;

DECLARE @Version decimal(5,2) = 1;

IF ((SELECT [dbo].[CheckVersion] (@Version)) = 0)
BEGIN
RAISERROR('CURRENT VERSION IS GREATER OR SAME THAN THE ONE YOU ARE TRYING TO EXECUTE', 20,-1) with log;
ROLLBACK;
END


--YOUR SQL CODE GOES HERE





--YOUR SQL CODE ENDS HERE


DELETE from DBVersion;
INSERT INTO DBVersion(VersionNumber) VALUES (@Version);

print 'SCRIPT EXECUTION COMPLETED SUCESSFULLY';

COMMIT;