﻿CREATE DATABASE [Pargo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Pargo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Pargo.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Pargo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Pargo_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [Pargo] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Pargo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Pargo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Pargo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Pargo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Pargo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Pargo] SET ARITHABORT OFF 
GO
ALTER DATABASE [Pargo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Pargo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Pargo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Pargo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Pargo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Pargo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Pargo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Pargo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Pargo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Pargo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Pargo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Pargo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Pargo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Pargo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Pargo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Pargo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Pargo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Pargo] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Pargo] SET  MULTI_USER 
GO
ALTER DATABASE [Pargo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Pargo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Pargo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Pargo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Pargo] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Pargo] SET QUERY_STORE = OFF
GO
ALTER DATABASE [Pargo] SET  READ_WRITE 
GO


CREATE TABLE [dbo].[DBVersion](
	[VersionNumber] [decimal](5,2) NOT NULL,
	[CreatedOn] [datetime] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DBVersion] ADD  CONSTRAINT [DF_DBVersion_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DBVersion] ADD  CONSTRAINT [DF_DBVersion_VersionNumber]  DEFAULT (1) FOR [VersionNumber]
GO



-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Boris Samardziev
-- Create date: 05-02-2018
-- Description:	Check Version Before Update
-- =============================================
CREATE FUNCTION CheckVersion 
(
	-- Add the parameters for the function here
	@version decimal(5,2)
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit = 0;

	DECLARE @dbVersion decimal(5,2) = 1;
	SET @dbVersion = (SELECT COALESCE((SELECT VersionNumber from DBVersion),1));
		
	IF (@version>@dbVersion)
		SET @Result = 1	
		
	-- Return the result of the function
	RETURN @Result;

END
GO
