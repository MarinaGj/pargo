﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore;
using ParGo.API.ViewModels;
using ParGo.Service;
using Microsoft.AspNetCore.Mvc;
using ParGo.Service.Interfaces;
using AutoMapper;
using ParGo.Model;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using SMSApi;

namespace ParGo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;
        private readonly ISMSSender _smsService;
        public AuthorizationController(IUserService userService, IConfiguration configuration, ISMSSender smsService)
        {
            _userService = userService;
            _smsService = smsService;
            _configuration = configuration;
        }

        // POST: api/Authorization/Register
       
        [Route("~/api/Authorization/Register")]
        [HttpPost]
        public async Task<ActionResult> Register([FromBody]UserVm userVm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }
            try
            {
                int randomCode = new Random().Next(1000, 10000);

                var user = Mapper.Map<UserVm, User>(userVm);
                user.VerificationCode = randomCode;

                var res = await _userService.Register(user);
                if (!res.IsFaulted)
                {
                    try
                    {
                        var result = await _smsService.SendMessage(randomCode.ToString(), userVm.phoneNumber);
                        if (result)
                        {
                            return Ok();
                        }
                        else
                        {
                            log4net.LogManager.GetLogger(this.GetType()).Error($"Verification code was not successfully sent to user with phone number: {userVm.phoneNumber}");
                            return BadRequest("Verification code was not successfully sent");
                        }
                    }
                    catch (Exception ex)
                    {
                        log4net.LogManager.GetLogger(this.GetType()).Error($"Error while sending {ex.Message}");
                        return BadRequest("Error while sending code.");
                    }
                }
                else
                {
                    return BadRequest(res.ErrorInfoAggregatedUserVisibleMessage);
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error($"Registration failed: {ex.Message}");
                return BadRequest("Registration failed.");
            }
        }

        // POST: api/Authorization/VerifyRegistration
       
        [Route("~/api/Authorization/VerifyRegistration")]
        [HttpPost]
        public async Task<ActionResult> ValidateRegistration([FromBody]UserVm userVm)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            var model = Mapper.Map<UserVm, User>(userVm);

            var res = await _userService.ValidateRegistration(model);

            if (!res.IsFaulted)
                return Ok();
            else
                return BadRequest(res.ErrorInfoAggregatedUserVisibleMessage);
        }

        // POST: api/Login
   
        [Route("~/api/Authorization/Login")]
        [HttpPost]
        public async Task<ActionResult> Login([FromBody]UserVm userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            var model = Mapper.Map<UserVm, Model.User>(userViewModel);

            var res = await _userService.Login(model);

            if (!res.IsFaulted)
                return Ok();
            else
                return BadRequest(res.ErrorInfoAggregatedUserVisibleMessage);
        }

        [Route("~/api/Authorization/UpdateUserData")]
        [HttpPost]
        public async Task<ActionResult> UpdateUserData([FromBody]UserVm userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid data.");
            }

            var model = Mapper.Map<UserVm, User>(userViewModel);
            model.ModifiedDate = DateTime.UtcNow;
            var res = await _userService.UpdateUserData(model);

            if (!res.IsFaulted)
                return Ok();
            else
                return BadRequest(res.ErrorInfoAggregatedUserVisibleMessage);
        }
    }
}
