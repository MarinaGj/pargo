﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ParGo.API.ViewModels
{
    public class UserVm
    {
        
        [Required]
        [StringLength(20)]
        public string phoneNumber { get; set; }

        [Required]
        [StringLength(20)]
        public string deviceId { get; set; }

        [StringLength(20)]
        public string licencePlate { get; set; }

        public int verificationCode { get; set; }

        [StringLength(100)]
        public string homeAddress { get; set; }

    }
}

