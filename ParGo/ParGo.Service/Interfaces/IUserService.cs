﻿using System.Threading.Tasks;
using ParGo.Model;
using ParGo.Shared.Validation;

namespace ParGo.Service.Interfaces
{
    public interface IUserService : IEntityService<User>
    {
        Task<MethodResult<User>> Register(User model);
        Task<MethodResult<User>> ValidateRegistration(User model);
        Task<MethodResult<User>> Login(User model);
        Task<MethodResult<User>> UpdateUserData(User model);
    }
}
