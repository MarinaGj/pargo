﻿using ParGo.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParGo.Service.Interfaces
{
    public interface IEntityService<T> : IService where T : BaseEntity
    {
        Task<int> Create(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task<ICollection<T>> GetAll();
    }
}
