﻿using Microsoft.EntityFrameworkCore;
using ParGo.Model;
using ParGo.Service.Interfaces;
using ParGo.Shared.Validation;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ParGo.Service
{
    public class UserService : EntityService<User>, IUserService
    {
        public UserService(PargoContext context) : base(context)
        {
            _context = context;
        }
        public async Task<MethodResult<User>> Register(User user)
        {
            var result = new MethodResult<User>(ErrorType.Business);
            try
            {
                var existingUsers = await _context.Users.Where(x => x.PhoneNumber == user.PhoneNumber && x.DeviceId == user.DeviceId /*&& x.LicencePlate == user.LicencePlate*/).ToListAsync();
                if (existingUsers != null && existingUsers.Any())
                {
                    result.ErrorInfos.Add(new ValidationError("Registration failed. User already exists!"));
                    return result;
                }
                user.Id = Guid.NewGuid();
                user.CreatedDate = DateTime.UtcNow;
             
                _context.Users.Add(user);
                var res = await _context.SaveChangesAsync() > 0;
                if (!res)
                {
                    result.ErrorInfos.Add(new ValidationError("Registration failed. User was not stored!"));
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ErrorInfos.Add(new ValidationError("Registration failed."));
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
                throw;
            }
        }

        public async Task<MethodResult<User>> ValidateRegistration(User user)
        {
            var result = new MethodResult<User>(ErrorType.Business);
            try
            {
                var userToBeVerified = await _context.Users.Where(x => x.PhoneNumber == user.PhoneNumber && x.DeviceId == user.DeviceId  /* && x.LicencePlate == user.LicencePlate*/ && x.VerificationCode == user.VerificationCode && !x.IsVerified).FirstOrDefaultAsync();
                if (userToBeVerified == null || userToBeVerified.Id == Guid.Empty)
                {
                    result.ErrorInfos.Add(new ValidationError("Verification failed! User is already verified or does not exist."));
                    return result;
                }
                userToBeVerified.IsVerified = true;
                _context.Users.Update(userToBeVerified);

                var res = await _context.SaveChangesAsync() > 0;
                if (!res)
                {
                    result.ErrorInfos.Add(new ValidationError("Verification failed. User was not stored!"));
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ErrorInfos.Add(new ValidationError("Verification failed."));
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
                throw;
            }
        }

        public async Task<MethodResult<User>> Login(User user)
        {
            var result = new MethodResult<User>(ErrorType.Business);
            try
            {
                var userToBeLoggedIn = await _context.Users.Where(x => x.PhoneNumber == user.PhoneNumber && x.DeviceId == user.DeviceId /*&& x.LicencePlate == user.LicencePlate */ && x.IsVerified).FirstOrDefaultAsync();
                if (userToBeLoggedIn == null || userToBeLoggedIn.Id == Guid.Empty)
                {
                    result.ErrorInfos.Add(new ValidationError("Login failed! User does not exist."));
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ErrorInfos.Add(new ValidationError("Verification failed."));
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
                throw;
            }
        }

        public async Task<MethodResult<User>> UpdateUserData(User user)
        {
            var result = new MethodResult<User>(ErrorType.Business);
            try
            {
                var userToBeUpdated = await _context.Users.Where(x => x.PhoneNumber == user.PhoneNumber && x.DeviceId == user.DeviceId /* && x.LicencePlate == user.LicencePlate*/ && x.IsVerified).FirstOrDefaultAsync();
                if (userToBeUpdated == null || userToBeUpdated.Id == Guid.Empty)
                {
                    result.ErrorInfos.Add(new ValidationError("Update of user data failed! User does not exist."));
                    return result;
                }

                userToBeUpdated.HomeAddress = user.HomeAddress;
                userToBeUpdated.LicencePlate = user.LicencePlate;
                userToBeUpdated.ModifiedDate = DateTime.UtcNow;

                _context.Users.Update(userToBeUpdated);

                var res = await _context.SaveChangesAsync() > 0;
                if (!res)
                {
                    result.ErrorInfos.Add(new ValidationError("Update of user data failed. User data were not stored!"));
                }
                return result;
            }
            catch (Exception ex)
            {
                result.ErrorInfos.Add(new ValidationError("Update of user data failed."));
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
                throw;
            }
        }
    }
}
