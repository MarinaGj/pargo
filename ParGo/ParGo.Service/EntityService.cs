﻿using Microsoft.EntityFrameworkCore;
using ParGo.Model;
using ParGo.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ParGo.Service
{
    public abstract class EntityService<T> : IEntityService<T> where T : BaseEntity
    {
        protected DbSet<T> _dbset;
        protected PargoContext _context;

        public EntityService(PargoContext context)
        {
            _context = context;
            _dbset = _context.Set<T>();
        }

        public async virtual Task<int> Create(T entity)
        {
            int result = -1;
            try
            {
                _dbset.Add(entity);
                result = await _context.SaveChangesAsync();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
            }
            return result;
        }

        public async virtual Task Update(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
            }
        }

        public async virtual Task Delete(T entity)
        {
            try
            {
                _dbset.Remove(entity);
                await _context.SaveChangesAsync();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
            }
        }

        public async virtual Task<ICollection<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }
    }
}
