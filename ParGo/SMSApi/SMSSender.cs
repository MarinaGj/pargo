﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace SMSApi
{
    public class SMSSender: ISMSSender
    {
        private readonly IConfiguration _configuration;
        private readonly string _accountSid;
        private readonly string _authToken;
        private readonly string _fromPhoneNumber;

        public SMSSender(IConfiguration configuration)
        {
            _configuration = configuration;
            _accountSid =  _configuration["AppConfiguration:TwilioAccountSID"];
            _authToken = _configuration["AppConfiguration:TwilioAuthToken"];
            _fromPhoneNumber = _configuration["AppConfiguration:TwilioFromNumber"];
            TwilioClient.Init(_accountSid, _authToken);
        }

        public async Task<bool> SendMessage(string messageBody, string toPhoneNumber)
        {
            try
            {
                var to = new PhoneNumber(toPhoneNumber);
                var message =  await MessageResource.CreateAsync(
                    to,
                    from: new PhoneNumber(_fromPhoneNumber), //  From number, must be an SMS-enabled Twilio number ( This will send sms from ur "To" numbers ). 
                    body: messageBody);

                log4net.LogManager.GetLogger(this.GetType()).Info($"Verification code sent to phoneNumber:{toPhoneNumber} successfully!");
                return true;
            }
            catch(Exception ex)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
            }
            return false;
        }
      
    }
}
