﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SMSApi
{
    public interface ISMSSender
    {
        Task<bool> SendMessage(string messageBody, string toPhoneNumber);
    }
}
