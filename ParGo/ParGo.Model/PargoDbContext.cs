﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using ParGo.Model.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace ParGo.Model
{
    [Serializable]
    public abstract class BaseEntity { }

    public abstract class Entity : BaseEntity, IEntity
    {
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //[BsonElement("Id")]
        [Key]
        public virtual Guid Id { get; set; }
    }

    public class PargoContext : DbContext, IContext
    {
        private IConfiguration _configuration;
        private IHttpContextAccessor _httpContextAccessor;
        //private static NRules.ISession factsSession;

        public PargoContext(IConfiguration configuration, IHttpContextAccessor httpContextAccessor) : base() //DbContextOptions<HoveDbContext> options : base(options) //, NRules.ISession session
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_configuration["Data:PargoConnection:ConnectionString"]); //ConnectionStrings:HoveConnection
                optionsBuilder.ConfigureWarnings(warnings => warnings.Ignore(CoreEventId.IncludeIgnoredWarning));
                optionsBuilder.ConfigureWarnings(x => x.Ignore(CoreEventId.DetachedLazyLoadingWarning));
                //optionsBuilder.UseLazyLoadingProxies();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.PhoneNumber).IsRequired().HasMaxLength(20);

                entity.Property(e => e.DeviceId).IsRequired().HasMaxLength(20);

                entity.Property(e => e.LicencePlate).HasMaxLength(20);

                entity.Property(e => e.HomeAddress).HasMaxLength(100);

                entity.Property(e => e.VerificationCode);

                entity.Property(e => e.IsVerified);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime").IsRequired();

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

            });

        }

        public DbSet<User> Users { get; set; }

        public async Task<int> SaveChangesAsync()
        {
            try
            {
                int result = 0;

                if (ChangeTracker.HasChanges())
                {
                    result = await base.SaveChangesAsync();
                }
                else
                    result = 1;

                return result;

            }
            catch (ValidationException e)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error(e.Message);
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(this.GetType()).Error(ex.Message);
                var inner = ex.InnerException;
                while (inner != null)
                {
                    log4net.LogManager.GetLogger(this.GetType()).Error(inner.Message);
                    inner = inner.InnerException;
                }
            }

            return 0;
        }
    }
}