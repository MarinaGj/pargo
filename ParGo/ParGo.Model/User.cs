﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ParGo.Model
{
    [Table("User")]
    public class User : Entity
    {
        public User()
        {
           
        }

        
        [Required]
        [StringLength(20)]
        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(20)]
        [DisplayName("Device ID")]
        public string DeviceId { get; set; }

        [StringLength(20)]
        [DisplayName("Licence Plate")]
        public string LicencePlate { get; set; }

        [StringLength(100)]
        [DisplayName("Home Address")]
        public string HomeAddress { get; set; }

        [DisplayName("Verification Code")]
        public int VerificationCode { get; set; }

        public bool IsVerified { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}