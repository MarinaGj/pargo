﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParGo.Model.Interfaces
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
