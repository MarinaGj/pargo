﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading.Tasks;

namespace ParGo.Model.Interfaces
{
    public interface IContext
    {
        Task<int> SaveChangesAsync();

        ChangeTracker ChangeTracker { get; }
    }
}
